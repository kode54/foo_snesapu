/**
 * foobar2000 wrapper for SNESAPU.dll
 *
 *     Licence :
 *        GNU General Public License
 *
 */

#pragma warning(disable : 4995)
#pragma warning(disable : 4996)

#include "stdafx.h"
#include "input_snesapu.hpp"
#include "id666.hpp"
#include "idex666.hpp"
#include "script700.hpp"
#include <mbstring.h>

#include "dll_manager.h"

static dll_manager m_dlls;

#define SPC_MAGIC "SNES-SPC700 Sound File Data "
#define DLL_NAME  "SNESAPU.DLL"
#define WRITE_SUPPORT

static const u32 SPC_MAX_CH = 8;

// larger values cause seeking distortion [ms]
static const u32 BUFFER_DURATION = 100;

// foobar2000-configurable variables
extern cfg_int cfg_samplerate;
extern cfg_int cfg_bitspersample;
extern cfg_int cfg_channels;
extern cfg_int cfg_interpolation;
extern cfg_int cfg_dsp_option;
extern cfg_int cfg_fastseek;
extern cfg_int cfg_usespclength;
extern cfg_int cfg_forcelength;
extern cfg_int cfg_songlength;
extern cfg_int cfg_fadelength;
extern cfg_int cfg_amplification;

/**
 * Get SNESAPU.DLL's kind.
 *
 * @return Original / Sunburst ver
 */
input_snesapu::SNESAPU_CORE input_snesapu::GetKind()
{
	if (!m_DllInst) {
		if ( !LoadDll() )
			throw exception_io_data("Unable to load SNESAPU.dll");
	}
	if (m_Apu.SetScript700) {
		return CORE_SSDLabo;
	}
	return CORE_ALPHA2;
}

/**
 * Load SNEAPU.DLL
 *
 * @return true / false
 */
bool input_snesapu::LoadDll()
{
	if (m_DllInst) {
		return true;
	}

	pfc::string8 path = core_api::get_my_full_path();
	path.truncate( path.scan_filename() );
	path += DLL_NAME;

	m_DllInst = m_dlls.load( path, true );

	::ZeroMemory(&m_Apu, sizeof(m_Apu));
	if (m_DllInst) {
		*(void**)&m_Apu.EmuAPU			= (void*)GetProcAddress(m_DllInst, "EmuAPU");
		*(void**)&m_Apu.GetAPUData		= (void*)GetProcAddress(m_DllInst, "GetAPUData");
		*(void**)&m_Apu.LoadSPCFile		= (void*)GetProcAddress(m_DllInst, "LoadSPCFile");
		*(void**)&m_Apu.SeekAPU			= (void*)GetProcAddress(m_DllInst, "SeekAPU");
		*(void**)&m_Apu.SetAPULength	= (void*)GetProcAddress(m_DllInst, "SetAPULength");
		*(void**)&m_Apu.SetAPUOpt		= (void*)GetProcAddress(m_DllInst, "SetAPUOpt");
		*(void**)&m_Apu.SetDSPAmp		= (void*)GetProcAddress(m_DllInst, "SetDSPAmp");
//		*(void**)&m_Apu.SetDSPEFBCT		= (void*)GetProcAddress(m_DllInst, "SetDSPEFBCT");
//		*(void**)&m_Apu.SetDSPPitch		= (void*)GetProcAddress(m_DllInst, "SetDSPPitch");
//		*(void**)&m_Apu.SetDSPStereo	= (void*)GetProcAddress(m_DllInst, "SetDSPStereo");
		*(void**)&m_Apu.SNESAPUInfo		= (void*)GetProcAddress(m_DllInst, "SNESAPUInfo");

		*(void**)&m_Apu.SetScript700	= (void*)GetProcAddress(m_DllInst, "SetScript700");
//		*(void**)&m_Apu.InitWork_700	= (void*)GetProcAddress(m_DllInst, "InitWork_700");

		m_Apu.GetAPUData(&m_Apu.ram, &m_Apu.xram, NULL, &m_Apu.t64Cnt,
						 &m_Apu.dsp, &m_Apu.voice, &m_Apu.vMMaxL, &m_Apu.vMMaxR);
	}

	if (m_DllInst == NULL) {
		console::info("Could not locate SNESAPU.DLL.");
		return false;
	}

	if (m_Apu.SNESAPUInfo == NULL || m_Apu.LoadSPCFile == NULL || m_Apu.EmuAPU == NULL) {
		console::info("SNESAPU.DLL function lookups failed.");
		return false;
	}

	u32 Version, Minimum, Options;
	m_Apu.SNESAPUInfo(&Version, &Minimum, &Options);
	if (Version < 0x20000 || Minimum > 0x20000) {
		console::info("Incompatible SNESAPU.DLL version.");
		FreeDll();
		return false;
	}

	return true;
}

/**
 * Free SNESAPU.DLL
 *
 */
void input_snesapu::FreeDll()
{
	if (!m_DllInst) {
		return;
	}

	if (m_DllInst) {
		m_dlls.free(m_DllInst);
		m_DllInst = NULL;
	}
}

/**
 * Destructor
 *
 */
input_snesapu::~input_snesapu()
{
	FreeDll();
}

/**
 * Get file information
 *
 * @param p_info
 * @param p_abort
 */
void input_snesapu::get_info(file_info &p_info, abort_callback &p_abort)
{
	if (m_CnfForceLength) {
		p_info.info_set_int("spc_songlength", m_TagTimeIntro / 1000);
		p_info.info_set_int("spc_fadelength", m_TagTimeFade);
		p_info.set_length(static_cast<double>(m_SongLength + m_FadeLength) / 1000.0);
	}
	if (m_TagSong.is_empty() == false) {
		p_info.meta_set("title", m_TagSong);
	}
	if (m_TagGame.is_empty() == false) {
		p_info.meta_set("album", m_TagGame);
	}
	if (m_TagArtist.is_empty() == false) {
		p_info.meta_set("artist", m_TagArtist);
	}
	if (m_TagDumper.is_empty() == false) {
		p_info.meta_set("dumper", m_TagDumper);
	}
	if (m_TagComment.is_empty() == false) {
		p_info.meta_set("comment", m_TagComment);
	}
	if (m_TagDate.is_empty() == false) {
		p_info.meta_set("date", m_TagDate);
	}
	if (m_TagPublisher.is_empty() == false) {
		p_info.meta_set("publisher", m_TagPublisher);
	}
	if (m_TagCopylight.is_empty() == false) {
		p_info.meta_set("copyright", m_TagCopylight);
	}
	if (m_TagOstName.is_empty() == false) {
		p_info.meta_set("ost", m_TagOstName);
	}
	if (m_TagOstDisc.is_empty() == false) {
		p_info.meta_set("discnumber", m_TagOstDisc);
	}
	if (m_TagOstTrack.is_empty() == false) {
		p_info.meta_set("tracknumber", m_TagOstTrack);
	}
	if (m_TagEmulater.is_empty() == false) {
		p_info.meta_set("emulator", m_TagEmulater);
	}
	if (m_TagAmp != -1) {
		p_info.info_set_int("spc_amplification", m_TagAmp);
	}

	p_info.info_set("codec", "SPC");
}

bool input_snesapu::decode_get_dynamic_info(file_info &p_out, double &p_timestamp_delta)
{
	if ( m_DynInfo ) {
		m_DynInfo = false;
		p_out.info_set_int("samplerate", m_CnfSampleRate);
		p_out.info_set_int("channels", m_CnfChannels);
		p_out.info_set_int("bitspersample", m_CnfBPS);
		p_timestamp_delta = -((double)m_LastDecoded / 1000.0);
		return true;
	}
	return false;
}

/**
 * Open file
 *
 * @param p_filehint
 * @param p_path
 * @param p_reason
 * @param p_abort
 *
 */
void input_snesapu::open(service_ptr_t<file> p_filehint, const char *p_path, t_input_open_reason p_reason, abort_callback &p_abort)
{
#if !defined(WRITE_SUPPORT)
	if (p_reason == input_open_info_write) {
		throw exception_io_unsupported_format();
	}
#endif

	m_File = p_filehint;
	m_SpcPath.set_string(p_path);
	input_open_file_helper(m_File, p_path, p_reason, p_abort);

	// read spc file
	m_SpcFileLength	= static_cast<u32>(m_File->get_size(p_abort));
	m_SpcFile.set_size(m_SpcFileLength);
	m_File->read(m_SpcFile.get_ptr(), m_SpcFileLength, p_abort);

	// check header
	if (::memcmp(m_SpcFile.get_ptr(), SPC_MAGIC, 28)) {
		throw exception_io_unsupported_format();
	}

	// load settings
	m_CnfSampleRate		= cfg_samplerate;
	m_CnfBPS			= cfg_bitspersample;
	m_CnfChannels		= cfg_channels;
	m_CnfMixing			= 3;
	m_CnfInterpolation	= cfg_interpolation;
	m_CnfOptions		= cfg_dsp_option;
	m_CnfFastSeek		= cfg_fastseek?true:false;
	m_CnfForceLength	= cfg_forcelength?true:false;
	m_CnfUseSpcLength	= cfg_usespclength?true:false;
	m_SongLength		= cfg_songlength;
	m_FadeLength		= cfg_fadelength;

	ReadSpcInfo();
}

/**
 * Initialize for decode
 *
 * @param p_flags
 * @param p_abort
 *
 */
void input_snesapu::decode_initialize(unsigned int p_flags, abort_callback &p_abort)
{
	if (LoadDll() == false) {
		throw exception_io_data("Unable to load SNESAPU.dll");
	}

	// load script700
	switch (GetKind()) {
	  case CORE_SSDLabo:
	  {
		script700 parser;
		if (parser.load(m_SpcPath) == true) {
			void *pScript = parser.get_ptr();
			if (pScript) {
				if (m_Apu.SetScript700(pScript) <= -1) {
					console::info("Script700 parse error");
				}
			}
		} else {
			m_Apu.SetScript700(NULL);
		}
		break;
	  }
	  default:
		break;
	}

	// send options/file to snesapu
	m_Apu.LoadSPCFile(m_SpcFile.get_ptr());

	m_Apu.SetAPUOpt(m_CnfMixing, m_CnfChannels, m_CnfBPS, m_CnfSampleRate, m_CnfInterpolation, m_CnfOptions);
	m_Apu.SetDSPAmp((m_TagAmp == 0xffffffff)?cfg_amplification:(m_TagAmp/0x1000));

	for (u32 i=0; i < SPC_MAX_CH; i++) {
		// mute
		m_Apu.voice[i].mFlg = ((m_TagMask >> i) & 0x01)?1:0;
		// init max volume
		m_Apu.voice[i].vMaxL = m_Apu.voice[i].vMaxR = 0;
	}

	*m_Apu.vMMaxL = *m_Apu.vMMaxR = 0;
	m_PlayedTime = 0;
	m_SilentTime = 0;

	if (m_CnfForceLength) {
		m_Apu.SetAPULength(m_SongLength * 64, m_FadeLength * 64);
	}
	// if finite playtime is wanted, enable forced length
	if (p_flags & input_flag_no_looping) {
		m_CnfUseSpcLength = m_CnfForceLength = true;
	}

	// allocate our output buffer
	u32 BufferLength = static_cast<u32>(((BUFFER_DURATION * m_CnfSampleRate) + BUFFER_DURATION) / 1000);
	BufferLength = BufferLength * m_CnfChannels * (m_CnfBPS / 8);
	m_DecodeBuffer.set_size(BufferLength);

	m_DynInfo = true;
	m_LastDecoded = 0;
}

/**
 * Decode
 *
 * @param p_chunk
 * @param p_abort
 * @return true / false
 */
bool input_snesapu::decode_run(audio_chunk &p_chunk, abort_callback &p_abort)
{
	// decide how many samples we want from sapu
	u32 wanted_ms = BUFFER_DURATION;

	// clip samples requested if nearing song end
	if (m_CnfForceLength) {
		if (m_PlayedTime >= (m_SongLength + m_FadeLength)) {
			return false;
		}
		
		if ((m_PlayedTime + wanted_ms) > (m_SongLength + m_FadeLength)) {
			wanted_ms = (m_SongLength + m_FadeLength) - m_PlayedTime;
		}
	}

	if (m_UseDefaultTime && m_SilentTime >= 5000) {
		return false;
	}
	
	// EmuAPU() wants no. of samples to render
	u32 wanted_sample = ((wanted_ms * m_CnfSampleRate) / 1000);
	m_Apu.EmuAPU(m_DecodeBuffer.get_ptr(), wanted_sample, 1);

	p_chunk.set_data_fixedpoint(
		m_DecodeBuffer.get_ptr(),
		wanted_sample * m_CnfChannels * (m_CnfBPS / 8),
		m_CnfSampleRate, m_CnfChannels ,m_CnfBPS,
		audio_chunk::g_guess_channel_config(m_CnfChannels));

	// check silent
	if (m_UseDefaultTime) {
		bool detect(false);
		for (u32 i = 0; i < SPC_MAX_CH; i++) {
			if ((m_Apu.dsp->voice[i].volL || m_Apu.dsp->voice[i].volR) && m_Apu.dsp->voice[i].envx) {
				detect = true;
				break;
			}
		}
		if (detect)
			m_SilentTime = 0;
		else
			m_SilentTime += wanted_ms;
	}

	m_PlayedTime += wanted_ms;

	m_LastDecoded = wanted_ms;

	return true;
}

/**
 * Seek
 *
 * @param p_seconds
 * @param p_abort
 */
void input_snesapu::decode_seek(double p_seconds, abort_callback &p_abort)
{
	// compare desired seek pos with current pos
	u32 NewPos = static_cast<u32>(p_seconds * 64 * 1000.0);

	m_DynInfo = true;
	m_LastDecoded = 0;

	// reset apu if necessary
	if (*m_Apu.t64Cnt >= NewPos) {
		m_Apu.LoadSPCFile(m_SpcFile.get_ptr());
		if (m_CnfForceLength) {
			m_Apu.SetAPULength(m_SongLength * 64, m_FadeLength * 64);
		}
	}
	m_Apu.SeekAPU((NewPos - *m_Apu.t64Cnt), m_CnfFastSeek?0xff:0);

	m_SilentTime = 0;
	m_PlayedTime = static_cast<u32>(p_seconds * 1000);
}

/**
 * Is seekeable?
 *
 * @return true / false
 */
bool input_snesapu::decode_can_seek()
{
	return m_CnfForceLength;
}

/**
 * Read ID666 tag
 *
 */
void input_snesapu::ReadID666()
{
	SPCHEAD *SpcHead = reinterpret_cast<SPCHEAD*>(m_SpcFile.get_ptr());

	pfc::stringcvt::string_utf8_from_ansi ansi_data(SpcHead->song, ID6_TEXT_MAX);
	m_TagSong.set_string(ansi_data);
	ansi_data.convert(SpcHead->game, ID6_TEXT_MAX);
	m_TagGame.set_string(ansi_data);
	ansi_data.convert(SpcHead->dumper, ID6_DUMPER_MAX);
	m_TagDumper.set_string(ansi_data);
	ansi_data.convert(SpcHead->comment, ID6_TEXT_MAX);
	m_TagComment.set_string(ansi_data);

	// Discrimination text / binary format
	if (m_SpcFile[0xd2] < 0x30) {
		// binary format
		if (SpcHead->bin.artist[0] != 0) {
			ansi_data.convert(SpcHead->bin.artist, ID6_TEXT_MAX);
			m_TagArtist.set_string(ansi_data);
		}
		if (SpcHead->bin.year && SpcHead->bin.year <= 9999) {
			pfc::string_printf date_format("%d%c%d%c%d", SpcHead->bin.year,'/',SpcHead->bin.month,'/',SpcHead->bin.date);
			m_TagDate.set_string(date_format);
		}
		// songlen
		if (SpcHead->bin.songlen) {
			m_TagTimeIntro = SpcHead->bin.songlen * 1000;
		}
		// fadelen
		int fadelen = SpcHead->bin.fadelen[0];
		fadelen += SpcHead->bin.fadelen[1] << 8;
		fadelen += SpcHead->bin.fadelen[2] <<16;
		if (fadelen) {
			m_TagTimeFade = fadelen;
		}
		// emulator
		if (SpcHead->bin.emulator >= 0 &&
			SpcHead->bin.emulator < XID6_EMULIST) {
			m_TagEmulater.set_string(emuList[SpcHead->bin.emulator]);
		}
	} else {
		// text format
		if (SpcHead->text.artist[0] != 0) {
			ansi_data.convert(SpcHead->text.artist, ID6_TEXT_MAX);
			m_TagArtist.set_string(ansi_data);
		}
		if (SpcHead->text.date[0] != 0) {
			ansi_data.convert(SpcHead->text.date, ID6_DATE_MAX);
			m_TagDate.set_string(ansi_data);
		}
		if (SpcHead->text.emulator >= 0x30 &&
			SpcHead->text.emulator < 0x30+XID6_EMULIST) {
			m_TagEmulater.set_string(emuList[SpcHead->text.emulator - 0x30]);
		}
		{
			char spc_songlength[4] = {0};
			char spc_fadelength[6] = {0};
			::CopyMemory(spc_songlength, SpcHead->text.songlen, 3);
			::CopyMemory(spc_fadelength, SpcHead->text.fadelen, 5);
			m_TagTimeIntro = atoi(spc_songlength) * 1000;
			m_TagTimeFade = atoi(spc_fadelength);
		}
	}
}

/**
 * Read extended ID666 tag
 *
 */
void input_snesapu::ReadExID666()
{
	if (m_SpcFileLength < 0x1020c) {
		return;
	}

	if (::memcmp(&m_SpcFile[OFS_XID6], XID6_MAGIC, 4) == 0) {
	    pfc::stringcvt::string_utf8_from_ansi ansi_data;
		s32 RemainSize = *(reinterpret_cast<s32*>(&m_SpcFile[OFS_XID6 + 4]));
		s32 RealSize = m_SpcFileLength - (OFS_XID6 + XID6_HEADER_LENGTH);

		if (RemainSize > RealSize) {
			RemainSize = RealSize;
		}

		XID6Chk *xid6 = reinterpret_cast<XID6Chk*>(&m_SpcFile[OFS_XID6 + XID6_HEADER_LENGTH]);
		while (RemainSize > 0) {
			RemainSize -= 4;
			s32 TagLength = (xid6->val + 3) & ~3;	// 4byte boudary

			switch (xid6->id) {
			  case XID6_SONG:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagSong.set_string(ansi_data);
				break;
			  case XID6_GAME:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagGame.set_string(ansi_data);
				break;
			  case XID6_ARTIST:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagArtist.set_string(ansi_data);
				break;
			  case XID6_PUB:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagPublisher.set_string(ansi_data);
				break;
			  case XID6_OST:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagOstName.set_string(ansi_data);
				break;
			  case XID6_DUMPER:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagDumper.set_string(ansi_data);
				break;
			  case XID6_CMNTS:
				ansi_data.convert((const char*)((&xid6->val)+1), xid6->val);
				m_TagComment.set_string(ansi_data);
				break;
			  case XID6_DISC:
				{
					pfc::string_printf disc("%d", xid6->val);
					m_TagOstDisc.set_string(disc);
				}
				break;
			  case XID6_TRACK:
				{
					if (xid6->val & 0xff) {
						pfc::string_printf track("%d%c", (xid6->val >> 8), (xid6->val & 0xff));
						m_TagOstTrack.set_string(track);
					} else {
						pfc::string_printf track("%d", (xid6->val >> 8));
						m_TagOstTrack.set_string(track);
					}
				}
				break;
			  case XID6_COPY:
				{
					pfc::string_printf copylight("%d", xid6->val);
					m_TagCopylight.set_string(copylight);
				}
				break;
			  case XID6_EMU:
				if (xid6->val >= 0x30 && xid6->val < 0x30+XID6_EMULIST) {
					m_TagEmulater.set_string(emuList[xid6->val - 0x30]);
				}
				break;
			  case XID6_DATE:
				{
					pfc::string_printf date_format("%d%c%d%c%d", xid6->data>>16,'/',(xid6->data>>8)&0xff,'/',xid6->data & 0xff);
					m_TagDate.set_string(date_format);
					break;
				}
			  case XID6_LOOPX:
				{
					m_TagLoopLimit = xid6->val;
					if (m_TagLoopLimit == 0)
					  m_TagLoopLimit = 1;
					if (m_TagLoopLimit > 9)
					  m_TagLoopLimit = 9;
				}
				break;
			  case XID6_FADE:
				m_TagTimeFade = xid6->data / 64;
				break;
			  case XID6_INTRO:
				m_TagTimeIntro = xid6->data / 64;
				break;
			  case XID6_LOOP:
				m_TagTimeLoop = xid6->data / 64;
				break;
			  case XID6_END:
				m_TagTimeEnd = xid6->data / 64;
				break;
			  case XID6_MUTE:
				m_TagMask = xid6->val & 0xff;
				break;
			  case XID6_AMP:
				if (xid6->type == XID6_TVAL) {
					m_TagAmp = (xid6->val << 12);
				} else if (xid6->type == XID6_TINT) {
					m_TagAmp =  xid6->data;
				}
				if (m_TagAmp <  32768) m_TagAmp = 32768;
				if (m_TagAmp > 524288) m_TagAmp = 524288;
				break;
			}
			if (xid6->type != XID6_TVAL) {
				xid6 = reinterpret_cast<XID6Chk*>(reinterpret_cast<u8*>(xid6) + TagLength);
				RemainSize -= TagLength;
			}
			xid6 = reinterpret_cast<XID6Chk*>(reinterpret_cast<u8*>(xid6) + 4);
		}
	}
}

/**
 *
 */
void input_snesapu::ReadSpcInfo()
{
	ReadID666();
	ReadExID666();

	// clamp song lengths
	if (m_TagTimeIntro > ID6_LENGTH_MAX_MS) {
		m_TagTimeIntro = ID6_LENGTH_MAX_MS;
	}
	if (m_TagTimeFade > ID6_FADE_MAX_MS) {
		m_TagTimeFade = ID6_FADE_MAX_MS;
	}
	// decide song length
	if ((m_TagTimeIntro||m_TagTimeLoop||m_TagTimeEnd) && m_CnfUseSpcLength) {
		m_CnfForceLength = true;
		m_UseDefaultTime = false;
		m_SongLength  = m_TagTimeIntro + (m_TagTimeLoop * m_TagLoopLimit) + m_TagTimeEnd;
		m_FadeLength  = m_TagTimeFade;
	}
}

void input_snesapu::WriteID666(const file_info &p_info, abort_callback &p_abort)
{
	bool IsText = (m_SpcFile[0xd2] < 0x30)?false:true;
	SPCHEAD Header;
	::CopyMemory(&Header, m_SpcFile.get_ptr(), 0x100);
	::ZeroMemory(reinterpret_cast<u8*>(&Header)+0x2c, 0x72);

	size_t TagMax = p_info.meta_get_count();
	for (size_t i=0; i < TagMax; i++) {
		pfc::string8 TagName = p_info.meta_enum_name(i);
		pfc::stringcvt::string_ansi_from_utf8 TagValue(p_info.meta_get(TagName, 0));

		if (::stricmp_utf8(TagName, "title") == 0) {
			::lstrcpynA(Header.song, TagValue.get_ptr(), ID6_TEXT_MAX);
			continue;
		}
		if (::stricmp_utf8(TagName, "album") == 0) {
			::lstrcpynA(Header.game, TagValue.get_ptr(), ID6_TEXT_MAX);
			continue;
		}
		if (::stricmp_utf8(TagName, "artist") == 0) {
			if (IsText) {
				::ZeroMemory(Header.text.artist, ID6_TEXT_MAX);
				::lstrcpynA(Header.text.artist, TagValue.get_ptr(), ID6_TEXT_MAX);
			} else {
				::ZeroMemory(Header.bin.artist, ID6_TEXT_MAX);
				::lstrcpynA(Header.bin.artist, TagValue.get_ptr(), ID6_TEXT_MAX);
			}
			continue;
		}
		if (::stricmp_utf8(TagName, "comment") == 0) {
			::lstrcpynA(Header.comment, TagValue.get_ptr(), ID6_TEXT_MAX);
			continue;
		}
		if (::stricmp_utf8(TagName, "dumper") == 0) {
			::lstrcpynA(Header.dumper, TagValue.get_ptr(), 16);
			continue;
		}
#if 0
		if (::stricmp_utf8(TagName, "date") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "emulator") == 0) {
			continue;
		}
#endif
	}
#if 0
	pfc::string8 SongLength = p_info.info_get("spc_songlength");
	if (!SongLength.is_empty()) {
		if (IsText) {
			::lstrcpynA(Header.text.songlen, SongLength.get_ptr(), 3);
		} else {
			int num = atoi(SongLength.get_ptr());
			Header.bin.songlen = num;
		}
	}
	pfc::string8 FadeLength = p_info.info_get("spc_fadelength");
	if (!FadeLength.is_empty()) {
		if (IsText) {
			::lstrcpynA(Header.text.fadelen, FadeLength.get_ptr(), 5);
		} else {
			int num = atoi(FadeLength.get_ptr());
			Header.bin.fadelen[0] = num       & 0xff;
			Header.bin.fadelen[1] = (num>> 8) & 0xff;
			Header.bin.fadelen[2] = (num>>16) & 0xff;
		}
	}
#endif
	m_File->seek(0, p_abort);
	m_File->write(reinterpret_cast<char*>(&Header), sizeof(Header), p_abort);
}

#if 0
size_t input_snesapu::WriteExID6(size_t p_Pos, XID6_ID p_Id, pfc::string_base p_Value)
{
	size_t Length = p_Value.length();
	size_t Mod = Length % 4;
	if (Mod) {
		Length += Mod;
	}

}
#endif

void input_snesapu::WriteExID666(const file_info &p_info, abort_callback &p_abort)
{
#if 0
	size_t WritePos = 0x1020c;
	size_t TagMax = p_info.meta_get_count();
	for (size_t i=0; i < TagMax; i++) {
		pfc::string8 TagName = p_info.meta_enum_name(i);
		pfc::stringcvt::string_ansi_from_utf8 TagValue(p_info.meta_get(TagName, 0));

		if (::stricmp_utf8(TagName, "title") == 0) {
//			WritePos += WriteExID6(WritePos, XID6_SONG, TagValue);
			continue;
		}
		if (::stricmp_utf8(TagName, "album") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "artist") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "comment") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "dumper") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "date") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "emulator") == 0) {
			continue;
		}
		if (::stricmp_utf8(TagName, "publisher") == 0) {
			// Extended id666
			continue;
		}
		if (::stricmp_utf8(TagName, "copyright") == 0) {
			// Extended id666
			continue;
		}
		if (::stricmp_utf8(TagName, "ost") == 0) {
			// Extended id666
			continue;
		}
		if (::stricmp_utf8(TagName, "ost_disc") == 0) {
			// Extended id666
			continue;
		}
		if (::stricmp_utf8(TagName, "ost_track") == 0) {
			// Extended id666
			continue;
		}
	}
//	m_File->truncate(WritePos, p_abort);
#endif
}

void input_snesapu::retag(const file_info &p_info, abort_callback &p_abort)
{
#if !defined(WRITE_SUPPORT)
	throw exception_io_unsupported_format();
#else
	WriteID666(p_info, p_abort);
	WriteExID666(p_info, p_abort);

	// Reread info
	m_File->seek(0, p_abort);
	m_SpcFileLength	= static_cast<u32>(m_File->get_size(p_abort));
	m_SpcFile.set_size(m_SpcFileLength);
	m_File->read(m_SpcFile.get_ptr(), m_SpcFileLength, p_abort);
	ReadSpcInfo();
#endif
}

static input_singletrack_factory_t<input_snesapu> g_input_snesapu_factory;

// version info
DECLARE_COMPONENT_VERSION("SNESAPU input",
                          "0.81",
                          "Uses SNESAPU.dll for SPC decoding.")
DECLARE_FILE_TYPE("SPC files", "*.spc");

						  // This will prevent users from renaming your component around (important for proper troubleshooter behaviors) or loading multiple instances of it.
VALIDATE_COMPONENT_FILENAME("foo_snesapu.dll");