//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by resource.rc
//
#define IDD_CONFIG                      101
#define IDC_SAMPLERATE                  1000
#define IDC_MIXING                      1001
#define IDC_INTERPOLATION               1002
#define IDC_DSP_ANALOG                  1003
#define IDC_DSP_OLDADPCM                1004
#define IDC_DSP_SURROUND                1005
#define IDC_DSP_REVERSE                 1006
#define IDC_DSP_DISECHO                 1007
#define IDC_FORCELENGTH                 1008
#define IDC_SONGLENGTH                  1009
#define IDC_FADELENGTH                  1010
#define IDC_BITS                        1011
#define IDC_CHANNELS                    1012
#define IDC_USESPCLENGTH                1013
#define IDC_FASTSEEK                    1014
#define IDC_VOLUME                      1015
#define IDC_DSP_BASSBOOST               1016
#define IDC_VOLUME_VALUE                1017
#define IDC_DSP_DISFIR                  1018
#define IDC_DSP_DISPITM                 1019
#define IDC_DSP_DISFIR3                 1020
#define IDC_DSP_DISPITR                 1020
#define IDC_DSP_DISENV                  1021
#define IDC_DSP_DISNOISE                1022
#define IDC_DSP_ECHOMEM                 1023
#define IDC_DSP_DISENV3                 1024
#define IDC_DSP_NOSURND                 1024

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
